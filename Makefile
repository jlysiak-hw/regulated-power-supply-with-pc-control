docs:
	mkdir -p docs
	./scripts/download.sh docs-links.txt

clean-kicad-bak:
	find . -name '*-bak' -delete

.PHONY: docs clean-kicad-bak
