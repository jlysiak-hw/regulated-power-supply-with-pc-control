# Regulated power supply with PC control

Project based on article from [ELEKTRONIKA PRAKTYCZNA 12/2017][ep-1217].
Unfortunately, it's in PL only.

## Construction

Brief description:

- KRADEX Z15 plastic enclosure
- EU/UK power cable (wall side), IEC C13 (device side)
- IEC C14 power socket (device side)
- toroidal transformer 200VA, 2x 14V/7.1A
- main power switch (DPST), panel mount
- fuse socket, panel mount
- 2 x banana sockets (output)
- 2 x rotary encoders with tact-switches
    - U / I control (encoder rotating)
    - short click: fine/coarse selection (per encoder)
    - long click: settings panel
- Buzzer
    - overheat
    - confirmation
    - overload
- display
    - (most probably) 20 x 4 character display connected to I2C PCF io expander
    - (maybe) OLED
- 4 LED status indicators
    - working
    - current limit (overload)
    - cooling
    - overheat

## uC selection

The original design used ATMega16 as a main uC. In DIL package - which is rather
huge nowadays. And probably, not much accessible.
In my design I'm trying to squash the PCB a bit but also wanted to use a cheap
and rich-enough (and accessible... yeah, I'm aware of global shortage)
microcontroller.

I did a quick research and it seems that there are plenty of Microchip uC on the
market. Some of them are pretty cheap, like €2 for 64-pin ARM-based uC with USB
(dev+host), many comm interfaces, IO, ADCs and timers - huh - sounds good!
Also, there are some MSP430 available but they are a bit less powerful.

After finishing the design of analog, power and control part I made a list of
minimal requirements for the uC:
- I2C BUS
- 2 x UART
- 3 x ADC (12bit or 10bit)
- 15 GPIO
    - 6 + 1 GPIO with RISE+FALL edge interrupt
    - 8 x GPIO ctrl
        - 4 x GPIO PUSH/PULL output (mosfet drivers)
        - 4 x GPIO OPEN-DRAIN (led drivers)
- 2 x PWM output

I've used [Microchip tools][microchip-tools] to find a fitting uC.
Btw, I feel that trying to use different chip & tools from different vendors
(other than ST) is quite interesting and it broaden you spectrum.
In Param Selection tool I've chosen 64-pin packages and applied some filters to
more-less match my requirements with respect to buses/IOs/ADCs/Timers.

I've found this one: https://www.microchip.com/en-us/product/ATSAMD21J16
It's also available in TME and damn cheap. :wink:

## Docs

[docs-links.txt][./docs-links.txt] contains links to various docs I was using
during the design and implementation process. Not included directly, because (1)
I'm not an owner, (2) PDFs usually consumes much space.

You can download all docs by running `make docs`.

## KiCAD design rules

- [OSHPark][oshpark]


[oshpark]:https://docs.oshpark.com/design-tools/kicad/kicad-design-rules/
[microchip-tools]:https://www.microchip.com/en-us/development-tools-tools-and-software/selection-tools
[ep-1217]:https://serwis.avt.pl/manuals/AVT5585.pdf
