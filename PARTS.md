# PARTS

1. Parts included in BOM.csv generated from KiCAD design.
2. Additional parts not stated in the KiCAD's BOM
    - toroidal transfomer TST 200/006, 2x14V 2x7.1A
        - https://www.tme.eu/pl/details/tts200_z230_14-14v/transformatory-toroidalne/breve-tufvassons/
    - plastic enclosure KRADEX Z15
        - https://www.tme.eu/pl/details/z-15_b/obudowy-z-panelem/kradex/z15/
        - https://www.tme.eu/Document/1db408c4d8cbc13bcb591115046a0116/Z15.PDF
    - Radiator A6023
        - https://www.tme.eu/pl/details/517-95ab/radiatory/wakefield-thermal/
        - https://www.tme.eu/pl/details/rad-a5723_100/radiatory/stonecold/
    - podkładki izolujące
    - pasta termoprzewodząca
    - buzzer 5V
    - fan RDH6025S
    - bolts and nuts M3
        - M3x5 https://www.tme.eu/pl/details/b3x5_bn30503/sruby/bossard/5401271/
        - M3x6 https://www.tme.eu/pl/details/b3x6_bn30503/sruby/bossard/5001994/
        - M3x8 https://www.tme.eu/pl/details/b3x8_bn30503/sruby/bossard/5002109/
        - M3 nut: https://www.tme.eu/pl/details/b3_bn124/nakretki/bossard/1090615/ 
    - power male socket IEC C14 :
      https://www.tme.eu/pl/details/iec-a-1/zlacza-iec-60320/adam-tech/
    - power cable C13
        - (EU plug):
          https://www.tme.eu/pl/details/ak-pc-01a/kable-zasil-komputerowe-i-uniwersalne/akyga/
        - UK plug 90deg:
          https://www.tme.eu/pl/details/sn24-3_07_1.5bk/kable-zasil-komputerowe-i-uniwersalne/lian-dung/
        - EU plug 90 deg:
          https://www.tme.eu/pl/details/cp117/kable-zasil-komputerowe-i-uniwersalne/logilink/ 
    - fuse socket (panel mount):
      https://www.tme.eu/pl/details/r3-38/gniazda-bezpiecznikowe-na-panel/sci/
    - fuse 6.3A:
      https://www.tme.eu/pl/details/zcm-6.3a/bezpieczniki-5x20mm-sredniozwloczne/eska/521-525/
    - gałki do enkoderów

