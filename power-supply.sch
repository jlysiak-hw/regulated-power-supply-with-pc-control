EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 1150 790  4240 2540
U 610482AB
F0 "output_stage" 50
F1 "output stage.sch" 50
$EndSheet
$Sheet
S 1250 4225 2300 1550
U 6110AC0B
F0 "main_power" 50
F1 "main_power.sch" 50
$EndSheet
$EndSCHEMATC
