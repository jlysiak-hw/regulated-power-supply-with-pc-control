# Some notes taken during design process

# Boards

I've decided to break out the original design into a couple of PCBs, which
makes a design more modular, easier to manufacture and maybe bit cheaper.
Also, some parts probably could be reused in other projects.
The design will not use any hi-speed signals between uC and regulation board
or uC and encoder board.

- Encoder board
    - 2 encoders should fit in 1 in^2 (it's 2.54x2.54 cm)
    - OSHPark has prototype service $5 per 1 in2 (for 3 boards), sounds good
- uC board
    - should also fit on 2 sided 1 in2 PCB
- power board

Display is a separate device connected via I2C.

# Encoder

## Board design goal
- small, max 1 in sq.
- 2 encoders with push-button
- holes for mounting screws
- small pin header for cable connection
    - signal interleaved with GND to reduce cross talk on cables

## Filtering signals from encoder

Datasheet suggest to use rc filer with 10k resistor and 0.01uF cap, plus 10k
pullup.


# uC
## Design checklist

In SAMD21 Family datasheet there is a section called `schematic checklist`,
which I really recommend to go through if designing a PCB with some SAMD uC for
the first time.

## Pinout table

Microchip datasheet seems to have quite clear description of pinout table.
At least for SAMD21.
However, some pins functionalities are not described in the main table, i.e.
oscillator pins.

## Power section

Datasheet, chapter 8 (p35)

Power pins:
* VDDIO: I/O lines, OSC8M and XOSC, 1.62V - 3.63V.
* VDDIN: I/O lines, internal regulator, 1.62V - 3.63V.
• VDDANA: I/O lines, ADC, AC, DAC, PTC, OSCULP32K, OSC32K,
    XOSC32K, 1.62V - 3.63V.
• VDDCORE: Internal regulated voltage output. Powers the core,
    memories, peripherals, FDPLL96M, and DFLL48M. Voltage is 1.2V.

The same voltage must be applied to both VDDIN, VDDIO and VDDANA (ref as VDD).
The ground pins, GND, are common to VDDCORE, VDDIO and VDDIN.
The ground pin for VDDANA is GNDANA.

### Decoupling power pins
- low ESR caps tied to each pin (closely)
- VDDCORE with 1uF cap
- power VDDANA via ferrite

## Assigning pins

Initial arrangements of IO pins started with noting down required connections:
- I2C BUS (2 pins)
- 2 x UART ( 4 pins )
- 4 x ADC inputs
- 7 EXTI 
- 6 IO drivers (4 PP, 4 OD)
- 2 PWM

## Online materials
- Microchip developer help: [LINK][microchip-dev]
    - includes also some bare metal programming examples for GCC


# Temperature sensors

The original design uses MCP970AE linear-output temperature sensor. However, at
the time of designing the board it wasn't available so I decided to use other
elements. As a base sensor I've chosen LM335 and just for learning purposes
I added extra I2C port for TC74A0 sensor with I2C communication bus. It's
already in TO220 package, so it should be easy to mount it to the radiator.


[microchip-dev]:https://microchipdeveloper.com/32arm:samd21-code-examples-listing
